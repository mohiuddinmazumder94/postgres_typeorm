import { Injectable } from '@nestjs/common';
import { CreateContinentDto } from './dto/create-continent.dto';
import { UpdateContinentDto } from './dto/update-continent.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Continent } from './entities/continent.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ContinentService {
  constructor(
    @InjectRepository(Continent)
    private readonly continentRepository: Repository<Continent>,
  ) {}

  async create(createContinentDto: CreateContinentDto) {
    const continent = this.continentRepository.create(createContinentDto);
    return await this.continentRepository.save(continent);
  }

  async findAll() {
    return await this.continentRepository.find({
      relations: {
        cities: true,
        countries: true,
      },
      select: {
        id: true,
        name: true,
        cities: {
          id: true,
          name: true,
        },
        conuntries: {
          id: true,
          name: true, 
        }
      },
    });
  }

  findOne(id: number) {
    return `This action returns a #${id} continent`;
  }

  update(id: number, updateContinentDto: UpdateContinentDto) {
    return `This action updates a #${id} continent`;
  }

  remove(id: number) {
    return `This action removes a #${id} continent`;
  }
}
