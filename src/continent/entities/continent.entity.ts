import { Country } from 'src/country/entities/country.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'continents' })
export class Continent {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  name: string;

  @OneToMany(() => Country, (country) => country.continent)
  countries: Country[];
}
