import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { join } from 'path';
import { CitiesModule } from './cities/cities.module';
import { ContinentModule } from './continent/continent.module';
import { CountryModule } from './country/country.module';
import { LeadersModule } from './leaders/leaders.module';
import { TimezoneModule } from './timezone/timezone.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        type: 'postgres',
        host: configService.get('DB_HOST'),
        port: +configService.get('DB_PORT'),
        username: configService.get('DB_USERNAME'),
        password: configService.get('DB_PASSWORD'),
        database: configService.get('DB_NAME'),
        entities: [join(process.cwd(), 'dist/**/*.entity.js')],
        //do not use synchronize: true in real projects
        synchronize: true,
      }),
    }),
    CitiesModule,
    ContinentModule,
    CountryModule,
    LeadersModule,
    TimezoneModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
