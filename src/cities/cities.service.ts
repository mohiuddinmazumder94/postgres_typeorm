import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCityDto } from './dto/create-city.dto';
import { UpdateCityDto } from './dto/update-city.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { City } from './entities/city.entity';
import { Brackets, Repository } from 'typeorm';
import { findCitiesDto } from './dto/find-cities.dto';

@Injectable()
export class CitiesService {
  constructor(
    @InjectRepository(City)
    private readonly citiesRepository: Repository<City>,
  ) {}

  async create(createCityDto: CreateCityDto) {
    const city = this.citiesRepository.create(createCityDto);
    return await this.citiesRepository.save(city);
  }

  async findAll(dto: findCitiesDto) {
    const { name, timezone, active, countryId, continentName } = dto;
    const conditions: Record<string, any> = {
      ...(name ? { name } : {}),
      ...(timezone ? { timezone } : {}),
      ...('active' in dto ? { active } : {}),
      ...(countryId ? { countryId } : {}),
      ...(continentName ? { continent: { name: continentName } } : {}),
    };
    return await this.citiesRepository.find({
      where: conditions,
      relations: {
        continent: true,
        country: true,
      },
      select: {
        id: true,
        name: true,
        timezone: true,
        continent: {
          id: true,
          name: true,
        },
      },
    });
  }

  async findMany(dto: findCitiesDto) {
    const { name, timezone, active, countryId, continentName, search } = dto;
    const queryBuilder = this.citiesRepository.createQueryBuilder('city');

    name && queryBuilder.andWhere('city.name = :name', { name });
    timezone &&
      queryBuilder.andWhere('city.timezone = :timezone', { timezone });
    'active' in dto &&
      queryBuilder.andWhere('city.active = :active', { active });
    countryId &&
      queryBuilder.andWhere('city.countryId = :countryId', { countryId });

    if (continentName) {
      queryBuilder.andWhere('continent.name = :continentName', {
        continentName,
      });
    }

    if (search) {
      //SELECT * FROM cities WHERE name = value AND  timezone = value AND OR OR OR
      queryBuilder.andWhere(
        new Brackets((qb) => {
          qb.where('LOWER(city.name) LIKE LOWER(:search)', {
            search: `%${search}%`,
          })
            .orWhere('LOWER(city.timezone) LIKE LOWER(:search)', {
              search: `%${search}%`,
            })
            .orWhere('LOWER(continent.name) LIKE LOWER(:search)', {
              search: `%${search}%`,
            });
        }),
      );
    }

    return queryBuilder
      .leftJoin('city.continent', 'continent')
      .select(['city.id', 'city.timezone', 'continent.name'])
      .getMany();
  }

  async findOne(id: number) {
    return await this.citiesRepository.findOne({
      where: {
        id,
      },
    });
  }

  async update(id: number, updateCityDto: UpdateCityDto) {
    const city = await this.findOne(id);
    if (!city) {
      throw new NotFoundException();
    }

    Object.assign(city, updateCityDto);

    return await this.citiesRepository.save(city);
  }

  async remove(id: number) {
    const city = await this.findOne(id);
    if (!city) {
      throw new NotFoundException();
    }

    return await this.citiesRepository.remove(city);
  }
}
