export class findCitiesDto {
  name?: string;
  timezone?: string;
  active?: boolean;
  countryId?: number;
  search?: string;
  continentName?: string;
}
