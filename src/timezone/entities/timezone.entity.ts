import { Country } from 'src/country/entities/country.entity';
import { Column, JoinColumn, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';

export class Timezone {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  zone: string;

  @ManyToMany(() => Country, (country) => country.timezones)
  countries: Country[];
}
