import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'country_timezone' })
export class CountryTimezone {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ default: 'active' })
  public status: string;
}
