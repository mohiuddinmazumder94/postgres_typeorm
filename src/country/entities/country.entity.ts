import { City } from 'src/cities/entities/city.entity';
import { Continent } from 'src/continent/entities/continent.entity';
import { Leader } from 'src/leaders/entities/leader.entity';
import { Timezone } from 'src/timezone/entities/timezone.entity';
import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'countries' })
export class Country {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  name: string;

  @ManyToOne(() => Continent, (continent) => continent.countries)
  continent: Continent;

  @OneToMany(() => City, (city) => city.country)
  cities: City[];

  @OneToOne(() => Leader, (leader) => leader.country)
  leader: Leader;

  @ManyToMany(() => Timezone, (timezone) => timezone.countries)
  @JoinTable({
    name: 'country_timezone',
    joinColumn: {
      name: 'country_id',
      referencedColumnName: 'id',
      foreignKeyConstraintName: 'country_timezone_country_id',
    },
    inverseJoinColumn: {
      name: 'timezone_id',
      referencedColumnName: 'id',
      foreignKeyConstraintName: 'country_timezone_timezone_id',
    },
  })
  timezones: Timezone[];
}
